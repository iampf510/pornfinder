/*
 * About auth
 * - Login
 * */

import api from '@/utils/request'
import qs from 'qs'

export function Login({email, password}){
    return api.request({
        method:'post',
        url:'/login',
        data:qs.stringify({
            email:email,
            password:password}),
        headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
        },
    })
}

export function Register({email, nickname, password}){
    return api.request({
        method:'post',
        url:'/register',
        data:qs.stringify({
            email:email,
            nickname:nickname,
            password:password}),
        headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
        },
    })
}
