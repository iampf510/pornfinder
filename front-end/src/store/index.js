import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

import { Login, Register } from '@/api/auth/auth.js'

export default new Vuex.Store({
    state: {
        token: localStorage.getItem('token') || '',
        isLogin: false,
        loadingInstance: null
    },
    mutations: {
        SET_USER_TOKEN(state, token){
            localStorage.setItem('token', JSON.stringify(token))
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
            if(token)
                state.isLogin = true
            else
                state.isLogin = false
        },

        CLEAR_USER_TOKEN (state) {
            localStorage.removeItem('token')
            location.reload()
        },

        SET_LOADING_INSTANCE(state, loadingInstance){
            state.loadingInstance = loadingInstance
        }

    },
    actions: {
        LOGIN({commit}, data) {
            return Login(data)
                .then(rsp => {
                    const token = rsp.data.token
                    localStorage.setItem('token', token)
                    commit('SET_USER_TOKEN', token)
                })
        },

        LOGOUT({commit}) {
            commit('CLEAR_USER_TOKEN')
        },

        REGISTER({commit},data) {
            return Register(data)
                .then(rsp => {
                    const token = rsp.data.token
                    localStorage.setItem('token', token)
                    commit('SET_USER_TOKEN', token)
                })
        }

    },
    modules: {
    }
})
