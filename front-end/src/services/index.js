import axios from 'axios'
import { Loading } from 'element-ui'
import store from '../store'

const request = axios.create({
  baseURL: '/api',
  headers: {
    Authorization: `Bearer ${store.state.token}`
  }
})

request.interceptors.request.use(config => {
  let loadingInstance = Loading.service()
  store.dispatch('setLoadingInstance', loadingInstance)
  config.headers.Authorization = `Bearer ${store.state.token}`
  return config
})
request.interceptors.response.use(response => {
  let loadingInstance = store.state.loadingInstance
  loadingInstance.close()
  return response
}, function (error) {
  let loadingInstance = store.state.loadingInstance
  loadingInstance.close()
  return Promise.reject(error)
})

export default request
