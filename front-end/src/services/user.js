import request from './index'

export default {
  register (data) {
    return request.post(
      '/user/register',
      data
    )
  },
  login (data) {
    const response = request.post(
      '/user/login',
      data
    )
    return response
  }
}
