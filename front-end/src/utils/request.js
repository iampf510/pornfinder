import axios from 'axios'

const BASEURL = "http://127.0.0.1:5000/api/"

const api = axios.create({
    baseURL : BASEURL,
    timeout : 5000
})

api.interceptors.request.use( config => {
    return config
}, error => {
    return Promise.reject(error)
});


api.interceptors.response.use( response => {
    return response
}, error => {
    return Promise.reject(error)
});

export default api
