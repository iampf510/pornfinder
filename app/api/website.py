from datetime import datetime, timedelta

from flask import jsonify, request

from . import api
from app.models import db
from app.models.website import Website

@api.route('/websites', methods=['GET'])
def get_websites_rule():
    websites = Website.query.all()
    if websites:
        return jsonify( [ website.get_rule() for website in websites] )
    return jsonify({})

@api.route('/website', methods=['POST'])
def add_websites_rule():
    data = request.get_json()

    website = Website(data['name'], data['rule'])
    db.session.add(website)
    db.session.commit()

    return jsonify({'code':'ok'})
