import os

from flask import Flask
# from flask_wtf.csrf import CsrfProtect
from flask_login import LoginManager
from flask_admin import Admin
from flask_caching import Cache
from flask_admin.contrib.sqla import ModelView
from flask_babelex import Babel
from flask_jwt_extended import JWTManager

from app.models.base import db

login_manager = LoginManager()
cache = Cache(config={'CACHE_TYPE': 'simple'})
admin = Admin(name='後台管理系統', template_mode='bootstrap3')
jwt = JWTManager()

def register_web_blueprint(app):
    from app.web import web
    app.register_blueprint(web)

def register_api_blueprint(app):
    from app.api import api
    #app.register_blueprint(api,url_prefix='/api')
    app.register_blueprint(api,url_prefix='/api')

def add_admin_model_view(admin):
    from app.models.user import User
    from app.models.website import Website 
    from app.models.video import Video
    from app.admin import UserView, WebsiteView, VideoView
    #admin.add_view(ModelView(User, db.session))
    admin.add_view(UserView(User, db.session))
    admin.add_view(WebsiteView(Website, db.session))
    admin.add_view(VideoView(Video, db.session))


def create_app(config=None):
    app = Flask(__name__, static_folder='../front-end/dist', static_url_path='')
    #app = Flask(__name__, static_url_path='')

    #: load default configuration
    app.config.from_object('app.settings')
    app.config.from_object('app.secure')

    babel = Babel(app)
    db.init_app(app)
    admin.init_app(app)
    jwt.init_app(app)


    login_manager.init_app(app)
    login_manager.login_view = 'web.login'
    login_manager.login_message = 'Login or register'

    cache.init_app(app)

    # 注册CSRF保护
    # csrf = CsrfProtect()
    # csrf.init_app(app)

    register_api_blueprint(app)
    register_web_blueprint(app)

    add_admin_model_view(admin)

    if config is not None:
        if isinstance(config, dict):
            app.config.update(config)
        elif config.endswith('.py'):
            app.config.from_pyfile(config)
    return app

