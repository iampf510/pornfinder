import datetime
DEBUG=True
BABEL_DEFAULT_LOCALE='zh_TW'
JWT_REFRESH_TOKEN_EXPIRES = datetime.timedelta(days=7)
