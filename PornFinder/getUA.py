#!/usr/bin/env python3
import threading
import queue
from requests_html import HTMLSession

def getUserAgent(f, urls):
    while not urls.empty():
        url = urls.get()
        global lock
        s = HTMLSession()
        r = s.get(url)
        uas = [ua.text for ua in r.html.find('.useragent a')] 

        lock.acquire()

        for ua in uas:
            f.write(f'{ua}\n')

        print(f'{url} done')
        lock.release()

def getUA(debug=False):
    f = open('ua-list', 'w')
    url = 'https://developers.whatismybrowser.com/useragents/explore/operating_system_name/windows/'
    s = HTMLSession()
    total = int(s.get(url).html.search('Last Page ({})')[-1])
    total = 500

    urls = queue.Queue()
    for i in range(1,total+1):
        urls.put(url+str(i))

    threads = []
    for i in range(30):
        thread = threading.Thread(target=getUserAgent, args=(f, urls,))
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

    f.close()


if __name__ == '__main__':
    UA = []
    lock = threading.Lock()
    getUA(debug=True)

