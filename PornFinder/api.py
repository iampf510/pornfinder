import requests

class Bot:
    def __init__(self, url, email, nickname, password):
        self.url = url
        self.data = {'email':email, 'nickname':nickname, 'password':password}
        self.headers = {}

    def register(self):
        rsp = requests.post(self.url + '/api/register', data=self.data).json()
        self.headers = { 'Authorization':'Bearer ' + rsp['token'] }

    def login(self):
        rsp = requests.post(self.url + '/api/login', self.data).json()

        if 'token' not in rsp:
            self.register()
        else:
            self.headers = { 'Authorization':'Bearer ' + rsp['token'] }





if __name__ == '__main__':
    url = 'http://127.0.0.1:5000'
    bot = Bot(url, 'aaa@aaa.aaa', 'aaa', 'aaaaaa')
    bot.register()
    bot.login()
    print(bot.headers)

